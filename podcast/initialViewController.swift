//
//  initialViewController.swift
//  podcast
//
//  Created by Karol on 16/02/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import UIKit

class initialViewController: UIViewController {

    @IBOutlet weak var name2: UITextField!
    @IBOutlet weak var test: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //zapisanie imienia w zmiennej UserDefaults dla klucza name
    @IBAction func goNext(_ sender: UIButton) {
        //test.text = "xd"
        UserDefaults.standard.set(name2.text, forKey: "name")
        performSegue(withIdentifier: "toMain", sender: self)
    }
    
}
