//
//  ViewController.swift
//  podcast
//
//  Created by Dawid Jenczewski on 28/01/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import UIKit
import WebRTC
class ViewController: UIViewController {

    @IBOutlet weak var welcome: UILabel!
    public var client:RTCClient?
    public var socket:Sockets?
    var room:Room?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Obcinanie rogow
        roomSettingsView.layer.cornerRadius = 15;
        createRoomView.layer.cornerRadius = 15;
        createRoomButton.layer.cornerRadius = 10;
        
        //wstawienie imienia na poczatku
        if let imie = UserDefaults.standard.value(forKey: "name"){
            welcome.text = "Hello  \(imie) !"
        }
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func createRoomTouched(_ sender: UIButton) {
        self.room = Room()
        self.room?.createRoom(roomID: roomName.text)
    }
    
    @IBAction func sendOffer(_ sender: Any) {
        //client?.makeOffer()
    }
    
    @IBAction func joinRoom(_ sender: UIButton) {
        self.room = Room()
        self.room?.joinRoom(roomID: roomName.text)
    }
    // dowiazanie twarde odpowiednio okno z ustawieniami, okno z Create Room na srodku, przycik Create Room roomSettingsView;
    @IBAction func disconnect(_ sender: UIButton) {
        //client?.disconnect()
    }
    @IBOutlet var roomSettingsView: UIView!
    @IBOutlet var createRoomView: UIView!
    @IBOutlet var createRoomButton: UIButton!
    //
    
    @IBOutlet weak var roomName: UITextField!
    
}

