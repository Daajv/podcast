//
//  Signaling.swift
//  podcast
//
//  Created by Dawid Jenczewski on 11/03/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import Foundation
import Starscream
//import SocketIO
import WebRTC;

public protocol Client:WebSocketClient{
    
}
public class Sockets{
    let socket:WebSocket?
    var peerConnection : RTCPeerConnection?
    var webRtcClient : RTCPeerConnectionFactory?
    var otherUsername:String = ""
    public var roomID:String = ""
    var username:String = UserDefaults.standard.value(forKey: "name") as? String ?? ""
    var rtcClient:RTCClient?
    let stunServer : String = "stun:stun.l.google.com:19302"
    enum MsgType: String {
        case login = "login"
        case offer = "offer"
        case answer = "answer"
        case candidate = "candidate"
        case createRoom = "createRoom"
        case joinRoom = "joinRoom"
        case userLogged = "userJoined"
        case leave = "leave"
        case error
        init(fromRawValue: String){
            self = MsgType(rawValue: fromRawValue) ?? .error
        }
        var description: String {
            return self.rawValue
        }
    }
    func Login(type: String!){
 
        let dict = ["type": type,
                    "room": self.roomID,
                    "name": self.username]
        if (socket!.isConnected){
            let jsonString = dict.json
            
            socket?.write(string: jsonString)
        }
        
    }
    func onMessage(_ msg : String) -> Void {
        let dict = msg.dictionary
        if dict.isEmpty {
            
            return
        }
        guard let type = dict["type"] as? String else {
            
            return
        }
        //let success = dict["success"] as! String
        let msgType : MsgType = MsgType.init(rawValue: type)!
        switch msgType {
        case .login:
            self.caseLogin(dict)
            break
        case .offer:
            self.caseOnOffer(dict)
            break
        case .answer:
            self.caseOnAnswer(dict)
            break
        case . candidate:
            self.caseOnCandidate(dict)
            break
            
        case .leave:
            //self.caseOnLeave(dict)
            print("client left")
            break
            
        case .error:
            print("error")
            break
            
        case .createRoom:
            self.caseOnCreateRoom(dict)
            break
        case .joinRoom:
            self.caseOnJoinRoom(dict)
            break
        case .userLogged:
            self.caseLogged(dict)
            break
        }
    }
    func caseOnOffer(_ dict : [String : Any]) -> Void {
        
        //print(dict)
        let typ:RTCSdpType?
        let name = dict["name"] as! String
        let sdpDict = dict["offer"] as! [String : Any]
        let type = sdpDict["type"] as! String
        let sdp = sdpDict["sdp"] as! String
        if(type == "offer") {typ = .offer}
        else {typ = .answer}
        let rtcSessionDesc = RTCSessionDescription.init(type: typ!, sdp: sdp)
        //print(rtcSessionDesc.debugDescription)
        //Potrzebna nazwa uzytkownika z textfielda
        //self.otherNameTextField.text = name
        self.otherUsername = name
        //self.peerConnection?.setRemoteDescriptionWith(self, sessionDescription: rtcSessionDesc!)
        //self.peerConnection?.createAnswer(with: self, constraints: nil)
        print("tu bylem")
        rtcClient?.createAnswerForOfferReceived(withRemoteSDP: rtcSessionDesc, roomID: self.roomID,name: name)
        
        
    }
    
    func caseLogged(_ dict: [String: Any]) -> Void{
        let name = dict["name"] as! String
        print("user" + name + " Logged")
        rtcClient?.makeOffer(name: name, roomID: roomID)
    }
    
    func caseLogin(_ dict : [String : Any]) -> Void {
        let isSuceess = dict["success"] as! Bool
        if (isSuceess == false){
        
            //show alert
            print(dict)
        }else {
           print("Joined the room")
            
            let dict = ["type" : "userJoined",
                        "room" : roomID,
            "name" : username] as [String : Any]
            socket?.write(string: dict.json)
        }
        
        
    }
    func caseOnCreateRoom(_ dict : [String : Any]) ->Void{
        
        let isSuccess = dict["success"] as! Bool
        if(isSuccess == false){
            print("Pokój o tej nazwie już istnieje")
        }else{
            print("Created room " + self.roomID)
        }
    }
    
    func caseOnJoinRoom(_ dict : [String : Any]) ->Void{
        let isSuccess = dict["success"] as! Bool
        if(isSuccess == false){
            print("Didn't find room")
        }else{
        }
    }
    func caseOnCandidate(_ dict : [String : Any]) -> Void {
        
        let name = dict["name"] as! String
        let candidateDict = dict["candidate"] as! [String : Any]
        let mid = candidateDict["sdpMid"] as! String
        let index = candidateDict["sdpMLineIndex"] as! Int32
        let sdp = candidateDict["candidate"] as! String // check what tag it is coming
        let candidate : RTCIceCandidate = RTCIceCandidate.init(sdp: sdp, sdpMLineIndex: index, sdpMid: mid)
        //self.peerConnection?.add(candidate)
        for peer in self.rtcClient!.peerConnection!{
            if(peer.name == name)
            {
                guard let pC = peer.peerConnection else {return}
                rtcClient?.addIceCandidate(iceCandidate: candidate, peerConnection: pC)
            }
        }
        
        //let candidate : RTCICECandidate = RTCICECandidate.init(mid: mid, index: index, sdp: sdp)
    }
    func caseOnAnswer(_ dict : [String : Any]) -> Void {
        
        print(dict)
        let typ:RTCSdpType?
        let name = dict["name"] as! String
        let sdpDict = dict["answer"] as! [String : Any]
        let type = sdpDict["type"] as! String
        let sdp = sdpDict["sdp"] as! String
        if(type == "offer") {typ = .offer}
        else {typ = .answer}
        let rtcSessionDesc = RTCSessionDescription.init(type: typ!, sdp: sdp)
         //self.peerConnection?.setRemoteDescriptionWith(self, sessionDescription: rtcSessionDesc!)
        rtcClient?.handleAnswerReceived(withRemoteSDP: rtcSessionDesc,name: name)
    }

    init(/*channelID:String*/){
        //self.channelID = "room1"
        self.socket = WebSocket(url: URL(string: "ws://192.168.0.2:9090")!)
        self.socket?.delegate = self as WebSocketDelegate
        self.socket?.connect()
    }
    
    deinit {
        self.socket?.disconnect()
    }
}
extension Sockets : WebSocketDelegate {
    public func websocketDidConnect(socket: WebSocketClient) {
        print("connected")
        //self.Login()
    }
    
    public func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("dicsonnected")
    }
    
    public func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        //print(text)
                if(text != "Hello world"){self.onMessage(text)}
    }
    
    public func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        
    }

    
    //MARK: WebSocketDelegate
    func websocketDidConnect(socket: WebSocket){
        print("connected")
        
    }
    func websocketDidDisconnect(socket: WebSocket, error: Error?){
        
        
        print(String(describing: error?.localizedDescription))
    }
    func websocketDidReceiveMessage(socket: WebSocket, text: String){
        
         print(text)
        if(text != "Hello world"){self.onMessage(text)}
        
    }
    func websocketDidReceiveData(socket: WebSocket, data: Data){
        
        
        print(data)
    }
    func peerConnection(_ peerConnection: RTCPeerConnection!, signalingStateChanged stateChanged: RTCSignalingState) {
          
      }
      
    
      func peerConnection(_ peerConnection: RTCPeerConnection!, addedStream stream: RTCMediaStream!) {
      }
      func peerConnection(_ peerConnection: RTCPeerConnection!, removedStream stream: RTCMediaStream!) {
          
      }
      func peerConnection(onRenegotiationNeeded peerConnection: RTCPeerConnection!) {
          
      }
      func peerConnection(_ peerConnection: RTCPeerConnection!, iceConnectionChanged newState: RTCIceConnectionState) {
      
      }
      func peerConnection(_ peerConnection: RTCPeerConnection!, iceGatheringChanged newState: RTCIceGatheringState) {
    
      }
}
