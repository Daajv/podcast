//
//  RTCPair.h
//  podcast
//
//  Created by Karol on 17/03/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

#ifndef RTCPair_h
#define RTCPair_h

#import <Foundation/Foundation.h>

// A class to hold a key and value.
@interface RTCPair : NSObject

@property(nonatomic, strong, readonly) NSString *key;
@property(nonatomic, strong, readonly) NSString *value;

// Initialize a RTCPair object with a key and value.
- (id)initWithKey:(NSString *)key value:(NSString *)value;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
// Disallow init and don't add to documentation
- (id)init __attribute__(
    (unavailable("init is not a supported initializer for this class.")));
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

@end
#endif /* RTCPair_h */
