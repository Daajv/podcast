//
//  Room.swift
//  podcast
//
//  Created by Dawid Jenczewski on 23/03/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import Foundation
import WebRTC;
import Starscream

public class Room{
    fileprivate var defaultChannel:Sockets
    fileprivate var usersChannels:[Sockets]?
    fileprivate var roomID: String?
    fileprivate var rtcClient:RTCClient
    //blednie
    let iceServer:RTCIceServer = RTCIceServer(urlStrings:["stun.l.google.com:19302","stun1.l.google.com:19302",
                                "stun2.l.google.com:19302","stun3.l.google.com:19302"])
    
    init(){
        self.defaultChannel = Sockets()
        self.rtcClient = RTCClient(iceServers: [iceServer])
    }
    
    public func createRoom(roomID : String?){
        while(!(self.defaultChannel.socket?.isConnected ?? true))
        {
            usleep(1)
        }
        self.defaultChannel.roomID = roomID ?? ""
        self.rtcClient.channel = self.defaultChannel
        self.defaultChannel.rtcClient = self.rtcClient
        self.defaultChannel.Login(type: "createRoom")
    }
    public func joinRoom(roomID : String?){
        while(!(self.defaultChannel.socket?.isConnected ?? true))
        {
            usleep(1)
        }
        self.defaultChannel.roomID = roomID ?? ""
        self.rtcClient.channel = self.defaultChannel
        self.defaultChannel.rtcClient = self.rtcClient
        self.defaultChannel.Login(type: "joinRoom")
    }
    
}
