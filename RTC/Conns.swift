//
//  Connections.swift
//  podcast
//
//  Created by Dawid Jenczewski on 27/02/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import Foundation
import WebRTC;



public enum RTCClientState {
    case disconnected
    case connecting
    case connected
}

public class PeerIdentity{
    public var name:String?
    public var roomID:String?
    public var peerConnection: RTCPeerConnection?
    public var connFactory: RTCPeerConnectionFactory?
    
    public func configure(defaultConnectionConstraint: RTCMediaConstraints,delegate: RTCPeerConnectionDelegate){
           initializePeerConnectionFactory()
           initializePeerConnection(defaultConnectionConstraint: defaultConnectionConstraint, delegate: delegate)
    }
    
    func initializePeerConnectionFactory(){
        RTCPeerConnectionFactory.initialize()
        self.connFactory = RTCPeerConnectionFactory()
        //print("cos")
    }
    
    func initializePeerConnection(defaultConnectionConstraint: RTCMediaConstraints,delegate: RTCPeerConnectionDelegate){
        RTCPeerConnection.initialize()
        let configuration = RTCConfiguration()
        //configuration.iceServers = self.iceServers
        self.peerConnection = self.connFactory!.peerConnection(with: configuration, constraints: defaultConnectionConstraint, delegate: delegate)
        if(self.peerConnection == nil) {print("niestety")}
    }
    
}


public protocol RTCClientDelegate: class {
    func rtcClient(client : RTCClient, startCallWithSdp sdp: RTCSessionDescription,type: String,roomID : String?,name :String?)
    func rtcClient(client : RTCClient, didReceiveLocalVideoTrack localVideoTrack: RTCVideoTrack)
    func rtcClient(client : RTCClient, didReceiveRemoteVideoTrack remoteVideoTrack: RTCVideoTrack)
    func rtcClient(client : RTCClient, didReceiveError error: Error)
    func rtcClient(client : RTCClient, didChangeConnectionState connectionState: RTCIceConnectionState)
    func rtcClient(client : RTCClient, didChangeState state: RTCClientState)
    func rtcClient(client : RTCClient, didGenerateIceCandidate iceCandidate: RTCIceCandidate)
    func rtcClient(client : RTCClient, didCreateLocalCapturer capturer: RTCCameraVideoCapturer)
}

extension RTCClientDelegate{
    // add default implementation to extension for optional methods
    func rtcClient(client : RTCClient, didReceiveError error: Error) {
        print(error)
    }

    func rtcClient(client : RTCClient, didChangeConnectionState connectionState: RTCIceConnectionState) {

    }

    func rtcClient(client : RTCClient, didChangeState state: RTCClientState) {
    }
    func rtcClient(client : RTCClient, startCallWithSdp sdp: RTCSessionDescription,type : String,roomID :String?, name: String?){
        print("przed" + type)
        if (type == "offer") {
            
            //self.peerConnection?.setLocalDescriptionWith(self, sessionDescription: sdp)
            let sdpDict : [String : String] = ["type" : type,
                                               "sdp" : sdp.description]
            //let sdpString = sdpDict.json
            let offerDict =  ["type" : "offer",
                              "offer" : sdpDict,
                              "name" : "karol"/*otherUsername*/] as [String : Any]
            client.channel?.socket?.write(string: offerDict.json)
            
            
        }else if (type == "answer") {
            
            //self.peerConnection?.setLocalDescriptionWith(self, sessionDescription: sdp)
            let sdpDict : [String : String] = ["sdp" : sdp.description,
                                               "type" : type]
             //let sdpString = sdpDict.json
            let offerDict =  ["type" : "answer",
                              "answer" : sdpDict,
                              "name" : "karol"/*client.otherUsername*/] as [String : Any]
            
            client.channel?.socket?.write(string: offerDict.json)
            
        }
    }
}

public class RTCClient:NSObject
{
    fileprivate var iceServers:[RTCIceServer] = []
    public var peerConnection:[PeerIdentity]? = []
    fileprivate var usersConnected:[String]?
    fileprivate var connFactory:RTCPeerConnectionFactory?
    fileprivate var audioTrack: RTCAudioTrack?
    fileprivate var isVideoCall = false
    fileprivate var dataChannel:RTCDataChannel?
    fileprivate var dataBuffer:RTCDataBuffer?
    fileprivate var remoteICECandidates: [RTCIceCandidate] = []
    fileprivate var sdpType:String = ""
    public var channel:Sockets?
    public weak var delegate: RTCClientDelegate?
    fileprivate let audioCallConstraint = RTCMediaConstraints(mandatoryConstraints: ["OfferToReceiveAudio" : "true"/*,"OfferToReceiveVideo" : "false"*/], optionalConstraints: nil)
    var callConstraint:RTCMediaConstraints{
        return self.audioCallConstraint
    }
    fileprivate let defaultConnectionConstraint = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: ["DtlsSrtpKeyAgreement" : "true"])
    
    private var state:RTCClientState = .connecting{
        didSet{
            self.delegate?.rtcClient(client: self, didChangeState: state)
        }
    }
    
    public override init(){
        super.init()
    }
    
   
    
    public convenience init(iceServers:[RTCIceServer])
    {
        self.init()
        self.iceServers = iceServers
        self.isVideoCall = false
        //self.configure()
    }

    deinit {
        
        //self.peerConnection
        for peer in self.peerConnection!{
            /*guard let peerConnection = peer else {
                return
            }*/
            if let stream = peer.peerConnection!.localStreams.first {
                audioTrack = nil
                peer.peerConnection!.remove(stream)
            }
        }
        
       }
    
    public func startConnection(pConnection : PeerIdentity) {
        guard let peerConnection = pConnection.peerConnection else {
            return
        }
      
        let localStream = self.localStream(peer: pConnection)
        peerConnection.add(localStream!)
        if let localVideoTrack = localStream!.videoTracks.first {
            self.delegate?.rtcClient(client: self, didReceiveLocalVideoTrack: localVideoTrack)
        }
          self.state = .connecting
    }
    
    public func disconnect(pConnection : RTCPeerConnection!) {
        guard let peerConnection = pConnection else {
            return
        }
        peerConnection.close()
        if let stream = peerConnection.localStreams.first {
            audioTrack = nil
            peerConnection.remove(stream)
        }
        self.delegate?.rtcClient(client: self, didChangeState: .disconnected)
    }
    
    func rtcClient(client : RTCClient, startCallWithSdp sdp: RTCSessionDescription,type : String,roomID :String?,name: String?){
        print("przed" + type)
        if (type == "offer") {
            
            let sdpDict : [String : String] = ["type" : type,
                                               "sdp" : sdp.sdp]
            let offerDict =  ["type" : "offer",
                              "offer" : sdpDict,
                              "room" : roomID!,
                              "name" : name!/*otherUsername*/] as [String : Any]
            client.channel?.socket?.write(string: offerDict.json)
            
            
        }else if (type == "answer") {
            
            //self.peerConnection?.setLocalDescriptionWith(self, sessionDescri
            let sdpDict : [String : String] = ["type" : type,
                                               "sdp" : sdp.sdp]
            // let sdpString = sdpDict.json
            let offerDict =  ["type" : "answer",
                              "answer" : sdpDict,
                              "room" : roomID!,
                              "name" : name!/*client.otherUsername*/] as [String : Any]
            
            client.channel?.socket?.write(string: offerDict.json)
            
        }
    }
    
    func rtcClient(pC : RTCPeerConnection, didGenerateIceCandidate: RTCIceCandidate){
        for peer in self.peerConnection!{
            if(peer.peerConnection == pC)
            {
                DispatchQueue.main.async {
                    if (didGenerateIceCandidate != nil) {
                    
                        let candidate = ["candidate" : didGenerateIceCandidate.sdp,
                                         "sdpMid" : didGenerateIceCandidate.sdpMid,
                                     "sdpMLineIndex" : didGenerateIceCandidate.sdpMLineIndex] as [String : Any]
                        let candidateDict  = ["type" : "candidate",
                                              "name" : peer.name!,
                                              "room" : peer.roomID!,
                                              "candidate": candidate] as [String : Any]
                        self.channel?.socket?.write(string: candidateDict.json)
                    }
                }
            }
        }
    }
    
    public func makeOffer(name : String?,roomID :String?) {
        let peer:PeerIdentity = PeerIdentity()
        peer.name = name
        peer.roomID = roomID
        peer.configure(defaultConnectionConstraint: defaultConnectionConstraint, delegate: self)
        self.startConnection(pConnection: peer)
        while(self.state != .connecting)
        {
            sleep(1)
        }
        sleep(5);
        self.peerConnection?.append(peer);
        
        
        guard let peerConnection = peer.peerConnection else {
            return
        }
        //print(peer.peerConnection.debugDescription)
        //print(peer.peerConnection?.localStreams.debugDescription)
        //
        
        peerConnection.offer(for: self.callConstraint, completionHandler: { [weak self]  (sdp, error) in
            guard let this = self else { return }
            if let error = error {
                print("blad")
                this.delegate?.rtcClient(client: this, didReceiveError: error)
            } else {
                this.sdpType = "offer"
                print(sdp!.sdp)
                this.handleSdpGenerated(sdpDescription: sdp,roomID: roomID,name:name)
            }
        })
    }
    
    public func handleAnswerReceived(withRemoteSDP remoteSdp: RTCSessionDescription?,name : String?) {
        guard let remoteSdp = remoteSdp else {
            return
        }
        for peer in self.peerConnection! {
        // Add remote description
            if(peer.name == name)
            {
                print("tu tez")
                guard let peerConnection = peer.peerConnection else{
                    print("blad")
                    return
                }
                let sessionDescription = RTCSessionDescription.init(type: .answer, sdp: remoteSdp.sdp)
                
                peerConnection.setRemoteDescription(sessionDescription, completionHandler: { [weak self] (error) in
                    guard let this = self else { return }
                    if let error = error {
                        this.delegate?.rtcClient(client: this, didReceiveError: error)
                    } else {
                        this.handleRemoteDescriptionSet(peer: peer)
                        this.state = .connected
                    }
                })
                break;
            }
        }
    }
    
    public func createAnswerForOfferReceived(withRemoteSDP remoteSdp: RTCSessionDescription?,roomID :String?,name: String?) {
        guard let remoteSdp = remoteSdp else {
                return
        }

        // Add remote description
        let peer:PeerIdentity = PeerIdentity()
               peer.name = name
               peer.roomID = roomID
               peer.configure(defaultConnectionConstraint: defaultConnectionConstraint, delegate: self)
               self.startConnection(pConnection: peer)
               self.peerConnection?.append(peer);
                let sessionDescription = RTCSessionDescription(type: .offer, sdp: remoteSdp.sdp)
                
                guard let peerConnection = peer.peerConnection else{
                    print("blad")
                    return
                }
                peerConnection.setRemoteDescription(sessionDescription, completionHandler: { [weak self] (error) in
                    guard let this = self else { return }
                    if let error = error {
                        this.delegate?.rtcClient(client: this, didReceiveError: error)
                    } else {
                        this.handleRemoteDescriptionSet(peer: peer)
                        // create answer
                        peerConnection.answer(for: this.callConstraint, completionHandler:
                            { (sdp, error) in
                                if let error = error {
                                    this.delegate?.rtcClient(client: this, didReceiveError: error)
                                } else {
                                    this.sdpType = "answer"
                                    print("answer")
                                    this.handleSdpGenerated(sdpDescription: sdp,roomID:roomID,name: name)
                                    this.state = .connected
                                }
                        })
                    }
                })
    }
    
    
    
    
    public func addIceCandidate(iceCandidate: RTCIceCandidate,peerConnection: RTCPeerConnection) {
           // Set ice candidate after setting remote description
           if peerConnection.remoteDescription != nil {
               peerConnection.add(iceCandidate)
           } else {
               self.remoteICECandidates.append(iceCandidate)
           }
       }

       public func muteCall(_ mute: Bool) {
           self.audioTrack?.isEnabled = !mute
       }    
}

public struct ErrorDomain {
    public static let videoPermissionDenied = "Video permission denied"
    public static let audioPermissionDenied = "Audio permission denied"
}

private extension RTCClient{
    
    func handleRemoteDescriptionSet(peer: PeerIdentity){
        guard let peerConnection = peer.peerConnection else{
            print("blad")
            return
        }
        for iceCandidate in self.remoteICECandidates
        {
            peerConnection.add(iceCandidate)
        }
        self.remoteICECandidates = []
    }
    
    func localStream(peer : PeerIdentity) -> RTCMediaStream?{
        guard let factory = peer.connFactory else{
            return nil
        }
        let localStream = factory.mediaStream(withStreamId: "RTCmS")
        if !AVCaptureState.isAudioDisabled{
            let audioTrack = factory.audioTrack(withTrackId: "RTCaS0")
            self.audioTrack = audioTrack
            localStream.addAudioTrack(audioTrack)
        }else{
            let error = NSError.init(domain: "Audio permission denied" , code: 0, userInfo: nil)
            print(error)
            self.delegate?.rtcClient(client: self, didReceiveError: error)
        }
        return localStream
    }
    
    
    
    func handleSdpGenerated(sdpDescription: RTCSessionDescription?, roomID:String?, name: String?) {
           guard let sdpDescription = sdpDescription  else {
                print("blad")
               return
           }
           // set local description
        for peer in self.peerConnection!{
            if(peer.name == name){
                
                guard let peerConnection = peer.peerConnection else{
                    print ("blad")
                    return
                }
                peerConnection.setLocalDescription(sdpDescription, completionHandler: {[weak self] (error) in
                    // issue in setting local description
                    guard let this = self, let error = error else { return }
                    this.delegate?.rtcClient(client: this, didReceiveError: error)
                })
                break;
            }
        }
            print("przeszlo")
           //  Signal to server to pass this sdp with for the session call
        //self.delegate?.rtcClient(client: self, startCallWithSdp: sdpDescription,type: sdpType)
        self.rtcClient(client: self, startCallWithSdp: sdpDescription,type: sdpType,roomID: roomID,name: name)
        //self.peerConnection(peerConnection, didCreateSessionDescription: <#T##RTCSessionDescription!#>, error: <#T##Error!#>)
       }
}

extension RTCClient{
    public func openDataChannel(){
        let configuration = RTCDataChannelConfiguration()
        configuration.channelId = 1
        configuration.maxRetransmits = 30
        configuration.maxPacketLifeTime = 30000
       // let dataChannel = self.peerConnection?.dataChannel(forLabel: "sharing files", configuration: configuration)
        //self.dataChannel = dataChannel
        self.dataChannel?.delegate = self as RTCDataChannelDelegate
    }
    public func getData(){
        
    }
}

extension RTCClient:RTCDataChannelDelegate{
    public func dataChannelDidChangeState(_ dataChannel: RTCDataChannel) {
        if(dataChannel.readyState == .closing)
        {
            dataChannel.close()
        }
    }
    
    public func dataChannel(_ dataChannel: RTCDataChannel, didReceiveMessageWith buffer: RTCDataBuffer) {
        
    }
    public func dataChannel(_ dataChannel: RTCDataChannel, didSendMessageWith bufer: RTCDataBuffer){
        dataChannel.sendData(bufer);
    }
    
}

extension RTCClient: RTCPeerConnectionDelegate {
    public func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
    }
    

    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        if stream.videoTracks.count > 0 {
            self.delegate?.rtcClient(client: self, didReceiveRemoteVideoTrack: stream.videoTracks[0])
        }
    }

    public func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {

    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        self.delegate?.rtcClient(client: self, didChangeConnectionState: newState)
        
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {

    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        self.rtcClient(pC: peerConnection, didGenerateIceCandidate: candidate)
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        
    }
    public func peerConnection(_ peerConnection: RTCPeerConnection!, didCreateSessionDescription sdp: RTCSessionDescription!) {
        
    }
    
}
